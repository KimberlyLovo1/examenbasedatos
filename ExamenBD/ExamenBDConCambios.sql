USE [BDExamen]
GO



/****** Object: StoredProcedure [dbo].[sp_getrole] Script Date: 24/11/2020 22:07:17 ******/
SET ANSI_NULLS ON
GO



SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[sp_getrole]
(
@username varchar(25),
@role int OUT
)
AS
BEGIN
Declare @loginName varchar (100)
Declare @user varchar (100)
DECLARE @rol varchar (25)



Select @loginName = @username
select @loginName=name from master.dbo.syslogins
where name = @loginName and dbname = 'BDExamen';
SELECT @loginName



SELECT @user = d.name
FROM sys.database_principals AS d
INNER JOIN sys.server_principals AS s
ON d.sid = s.sid
WHERE s.name = @loginName;



SELECT @rol = r.name
FROM sys.database_role_members AS m
INNER JOIN sys.database_principals AS r
ON m.role_principal_id = r.principal_id
INNER JOIN sys.database_principals AS u
ON u.principal_id = m.member_principal_id
WHERE u.name = @user;



if(@rol = 'db_ddladmin')
begin
set @role = 1;
end
ELSE IF (@rol = 'db_datawriter')
begin
SET @role = 2;
end
ELSE IF (@rol = 'db_datareader')
begin
SET @role = 3;
end
select @role
END;
GO