﻿namespace ExamenBD
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTablas = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTipo = new System.Windows.Forms.Button();
            this.btnIVentas = new System.Windows.Forms.Button();
            this.btnIProductos = new System.Windows.Forms.Button();
            this.btnICompras = new System.Windows.Forms.Button();
            this.btnIProveedor = new System.Windows.Forms.Button();
            this.btnICliente = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnView1 = new System.Windows.Forms.Button();
            this.btnView2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.btnTablas);
            this.groupBox1.Location = new System.Drawing.Point(19, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 57);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listar";
            // 
            // btnTablas
            // 
            this.btnTablas.Location = new System.Drawing.Point(24, 21);
            this.btnTablas.Name = "btnTablas";
            this.btnTablas.Size = new System.Drawing.Size(96, 30);
            this.btnTablas.TabIndex = 0;
            this.btnTablas.Text = "Tablas";
            this.btnTablas.UseVisualStyleBackColor = true;
            this.btnTablas.Click += new System.EventHandler(this.btnTablas_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox2.Controls.Add(this.btnTipo);
            this.groupBox2.Controls.Add(this.btnIVentas);
            this.groupBox2.Controls.Add(this.btnIProductos);
            this.groupBox2.Controls.Add(this.btnICompras);
            this.groupBox2.Controls.Add(this.btnIProveedor);
            this.groupBox2.Controls.Add(this.btnICliente);
            this.groupBox2.Location = new System.Drawing.Point(19, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(183, 239);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Insertar";
            // 
            // btnTipo
            // 
            this.btnTipo.Location = new System.Drawing.Point(18, 194);
            this.btnTipo.Name = "btnTipo";
            this.btnTipo.Size = new System.Drawing.Size(129, 26);
            this.btnTipo.TabIndex = 9;
            this.btnTipo.Text = "Tipo Producto";
            this.btnTipo.UseVisualStyleBackColor = true;
            this.btnTipo.Click += new System.EventHandler(this.btnTipo_Click);
            // 
            // btnIVentas
            // 
            this.btnIVentas.Location = new System.Drawing.Point(18, 130);
            this.btnIVentas.Name = "btnIVentas";
            this.btnIVentas.Size = new System.Drawing.Size(129, 26);
            this.btnIVentas.TabIndex = 5;
            this.btnIVentas.Text = "Ventas";
            this.btnIVentas.UseVisualStyleBackColor = true;
            this.btnIVentas.Click += new System.EventHandler(this.btnIVentas_Click);
            // 
            // btnIProductos
            // 
            this.btnIProductos.Location = new System.Drawing.Point(18, 162);
            this.btnIProductos.Name = "btnIProductos";
            this.btnIProductos.Size = new System.Drawing.Size(129, 26);
            this.btnIProductos.TabIndex = 4;
            this.btnIProductos.Text = "Productos";
            this.btnIProductos.UseVisualStyleBackColor = true;
            this.btnIProductos.Click += new System.EventHandler(this.btnIProductos_Click);
            // 
            // btnICompras
            // 
            this.btnICompras.Location = new System.Drawing.Point(18, 95);
            this.btnICompras.Name = "btnICompras";
            this.btnICompras.Size = new System.Drawing.Size(129, 26);
            this.btnICompras.TabIndex = 3;
            this.btnICompras.Text = "Compras";
            this.btnICompras.UseVisualStyleBackColor = true;
            this.btnICompras.Click += new System.EventHandler(this.btnICompras_Click);
            // 
            // btnIProveedor
            // 
            this.btnIProveedor.Location = new System.Drawing.Point(18, 63);
            this.btnIProveedor.Name = "btnIProveedor";
            this.btnIProveedor.Size = new System.Drawing.Size(129, 26);
            this.btnIProveedor.TabIndex = 2;
            this.btnIProveedor.Text = "Proveedor";
            this.btnIProveedor.UseVisualStyleBackColor = true;
            this.btnIProveedor.Click += new System.EventHandler(this.btnIProveedor_Click);
            // 
            // btnICliente
            // 
            this.btnICliente.Location = new System.Drawing.Point(18, 31);
            this.btnICliente.Name = "btnICliente";
            this.btnICliente.Size = new System.Drawing.Size(129, 26);
            this.btnICliente.TabIndex = 1;
            this.btnICliente.Text = "Clientes";
            this.btnICliente.UseVisualStyleBackColor = true;
            this.btnICliente.Click += new System.EventHandler(this.btnICliente_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox3.Controls.Add(this.btnView2);
            this.groupBox3.Controls.Add(this.btnView1);
            this.groupBox3.Location = new System.Drawing.Point(208, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(135, 302);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reporte";
            // 
            // btnView1
            // 
            this.btnView1.Location = new System.Drawing.Point(16, 106);
            this.btnView1.Name = "btnView1";
            this.btnView1.Size = new System.Drawing.Size(88, 61);
            this.btnView1.TabIndex = 0;
            this.btnView1.Text = "Ventas por cliente";
            this.btnView1.UseVisualStyleBackColor = true;
            this.btnView1.Click += new System.EventHandler(this.btnView1_Click_1);
            // 
            // btnView2
            // 
            this.btnView2.Location = new System.Drawing.Point(16, 33);
            this.btnView2.Name = "btnView2";
            this.btnView2.Size = new System.Drawing.Size(88, 67);
            this.btnView2.TabIndex = 1;
            this.btnView2.Text = "Producto más vendido";
            this.btnView2.UseVisualStyleBackColor = true;
            this.btnView2.Click += new System.EventHandler(this.btnView1_Click);
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(355, 338);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnIVentas;
        private System.Windows.Forms.Button btnIProductos;
        private System.Windows.Forms.Button btnICompras;
        private System.Windows.Forms.Button btnIProveedor;
        private System.Windows.Forms.Button btnICliente;
        private System.Windows.Forms.Button btnTipo;
        private System.Windows.Forms.Button btnTablas;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnView2;
        private System.Windows.Forms.Button btnView1;
    }
}