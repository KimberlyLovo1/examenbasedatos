﻿using ExamenBD.Añadir;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.MuestraBusqueda;
using ExamenBD.Reporte;

namespace ExamenBD
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        //Insertar
        private void btnICliente_Click(object sender, EventArgs e)
        {
            AñadCliente c = new AñadCliente();
            c.Visible = true; 
        }

        private void btnICompras_Click(object sender, EventArgs e)
        {
            AñadCompra cp = new AñadCompra();
            cp.Visible = true;
        }

        private void btnIProveedor_Click(object sender, EventArgs e)
        {
            AñadProveedor p = new AñadProveedor();
            p.Visible = true;
        }

        private void btnIVentas_Click(object sender, EventArgs e)
        {
            AñadVenta v = new AñadVenta();
            v.Visible = true;
        }

        private void btnIProductos_Click(object sender, EventArgs e)
        {
            AñadProducto pd = new AñadProducto();
            pd.Visible = true;
        }

        private void btnTipo_Click(object sender, EventArgs e)
        {
            Tipo t = new Tipo();
            t.Visible = true;
        }

        private void btnTablas_Click(object sender, EventArgs e)
        {
            TablaPrincipal tp = new TablaPrincipal();
            tp.Visible = true;
        }

        private void btnView1_Click(object sender, EventArgs e)
        {
            view2 v = new view2();
            v.Visible = true;
        }

        private void btnView1_Click_1(object sender, EventArgs e)
        {
            view1 v = new view1();
            v.Visible = true;

        }
    }
}
