﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.MuestraBusqueda
{
    public partial class TablaPrincipal : Form
    {
        public TablaPrincipal()
        {
            InitializeComponent();
            //cmbLista.Items.Add("");
            
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cmbLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbLista.SelectedIndex == 0)
            {
               dgvTabla.DataSource = Mostrar.MostrarClientes();
            }
            if(cmbLista.SelectedIndex == 1)
            {
                dgvTabla.DataSource = Mostrar.MostrarProductos();
            }
            if(cmbLista.SelectedIndex == 2)
            {
                dgvTabla.DataSource = Mostrar.MostrarProveedores();
            }
            if(cmbLista.SelectedIndex == 3)
            {
                dgvTabla.DataSource = Mostrar.MostrarCompra();
            }
            if(cmbLista.SelectedIndex == 4)
            {
                dgvTabla.DataSource = Mostrar.DetalleCompra();
            }
            if (cmbLista.SelectedIndex == 5)
            {
                dgvTabla.DataSource = Mostrar.Ventas();
            }
            if (cmbLista.SelectedIndex == 6)
            {
                dgvTabla.DataSource = Mostrar.DetalleVenta();
            }
            if (cmbLista.SelectedIndex == 7)
            {
                dgvTabla.DataSource = Mostrar.Devolu();
            }
            if (cmbLista.SelectedIndex == 8)
            {
                dgvTabla.DataSource = Mostrar.DetalleDevolu();
            }
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            if(cmbLista.SelectedIndex == 0)
            {
                dgvTabla.DataSource = Busqueda.BusquedaCliente(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 1)
            {
                dgvTabla.DataSource = Busqueda.BusquedaProducto(Convert.ToString(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 2)
            {
                dgvTabla.DataSource = Busqueda.BusquedaProveedor(Convert.ToString(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 3)
            {
                dgvTabla.DataSource = Busqueda.BusquedaCompra(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 4)
            {
                dgvTabla.DataSource = Busqueda.BusquedaDetalleC(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 5)
            {
                dgvTabla.DataSource = Busqueda.BusquedaVenta(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 6)
            {
                dgvTabla.DataSource = Busqueda.BusquedaDetalleV(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 7)
            {
                dgvTabla.DataSource = Busqueda.BusquedaDevolucion(Convert.ToInt32(txtBusqueda));
            }
            if (cmbLista.SelectedIndex == 8)
            {
                dgvTabla.DataSource = Busqueda.BusquedaDetalleDevo(Convert.ToInt32(txtBusqueda));
            }
        }
    }
}
