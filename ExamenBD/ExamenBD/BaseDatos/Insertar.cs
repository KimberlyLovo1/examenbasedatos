﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenBD.BaseDatos
{
    class Insertar
    {
        public static int InsertarCliente(String PrimNombre, String SegunNombre, String PrimApell, String SegunApell, String telef)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.Ncliente(PrimNombre, SegunNombre, PrimApell, SegunApell, telef);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarProveedor(String id_Prov, String Nombre, String Telef, String Direcc, String Correo)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.NProveedores(id_Prov, Nombre, Telef, Direcc, Correo);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarProductos(String codProd, String Nombre, String Direcc, float Precio, int Exist, String Id_prov, int TipoProv)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.NProductos(codProd, Nombre, Direcc, Precio, Exist, Id_prov, TipoProv);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarTipoProd(String nombre)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.IngresarTipoProd(nombre);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        
        public static int InsertarVenta(int id_Cliente)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.Nventas(id_Cliente);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarDetalleVent(int idVent, String codProd, int cant)
        {
            try
            {
                using (var Ins = new BDExamenEntities())
                {
                    return Ins.NDetalleVenta(idVent, codProd, cant);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarCompra( String Id_Prov, String TipoPago)
        {
            try
            {
                using (var db = new BDExamenEntities())
                {
                    return db.Ncompra(Id_Prov, TipoPago);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertDetalleComp(int idComp, String codProd, int cant, float precio)
        {
            try
            {
                using (var db = new BDExamenEntities())
                {
                    return db.NDetalleCompra(idComp, codProd, cant, precio);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

    }
}
