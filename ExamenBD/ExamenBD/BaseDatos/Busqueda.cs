﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenBD.BaseDatos
{
    class Busqueda
    {
        public static List<BuscarCl_Result> BusquedaCliente(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarCl(id).ToList();
                }
            }
            catch(Exception)
            {
                return null;
            }
        }
        public static List<BuscarCompra_Result> BusquedaCompra(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarCompra(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarDetallecompra_Result> BusquedaDetalleC(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarDetallecompra(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscardetalleDevolucion_Result> BusquedaDetalleDevo(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscardetalleDevolucion(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarDetalleVenta_Result> BusquedaDetalleV(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarDetalleVenta(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarDevolu_Result> BusquedaDevolucion(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarDevolu(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarProduc_Result> BusquedaProducto(string id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarProduc(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarProv_Result> BusquedaProveedor(string id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarProv(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static List<BuscarVentas_Result> BusquedaVenta(int id)
        {
            try
            {
                using (var b = new BDExamenEntities())
                {
                    return b.BuscarVentas(id).ToList();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
