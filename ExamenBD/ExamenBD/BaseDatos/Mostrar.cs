﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenBD.BaseDatos
{
    class Mostrar
    {
        public static List<ListarC_Result> MostrarClientes()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarC().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarCompras_Result> MostrarCompra()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarCompras().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarProduct_Result> MostrarProductos()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarProduct().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarProv_Result> MostrarProveedores()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarProv().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Listardeatlledevolucion_Result> DetalleDevolu()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.Listardeatlledevolucion().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Listardetallecompras_Result> DetalleCompra()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.Listardetallecompras().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarDetalle_Venta_Result> DetalleVenta()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarDetalle_Venta().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarDevolu_Result> Devolu()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarDevolu().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarTipoProd_Result> TipoProd()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarTipoProd().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ListarVentas_Result> Ventas()
        {
            try
            {
                using (var m = new BDExamenEntities())
                {
                    return m.ListarVentas().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
