﻿namespace ExamenBD.Añadir
{
    partial class AñadCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrimNomb = new System.Windows.Forms.TextBox();
            this.txtSegunNomb = new System.Windows.Forms.TextBox();
            this.txtPrimApell = new System.Windows.Forms.TextBox();
            this.txtSegunApell = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTelef = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(223, 238);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(83, 25);
            this.btnCancelar.TabIndex = 0;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(125, 238);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(81, 25);
            this.btnAceptar.TabIndex = 1;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Primer Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Segundo Nombre:";
            // 
            // txtPrimNomb
            // 
            this.txtPrimNomb.Location = new System.Drawing.Point(192, 47);
            this.txtPrimNomb.Name = "txtPrimNomb";
            this.txtPrimNomb.Size = new System.Drawing.Size(206, 22);
            this.txtPrimNomb.TabIndex = 4;
            this.txtPrimNomb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrimNomb_KeyPress);
            // 
            // txtSegunNomb
            // 
            this.txtSegunNomb.Location = new System.Drawing.Point(192, 81);
            this.txtSegunNomb.Name = "txtSegunNomb";
            this.txtSegunNomb.Size = new System.Drawing.Size(206, 22);
            this.txtSegunNomb.TabIndex = 5;
            this.txtSegunNomb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSegunNomb_KeyPress);
            // 
            // txtPrimApell
            // 
            this.txtPrimApell.Location = new System.Drawing.Point(171, 95);
            this.txtPrimApell.Name = "txtPrimApell";
            this.txtPrimApell.Size = new System.Drawing.Size(206, 22);
            this.txtPrimApell.TabIndex = 6;
            this.txtPrimApell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrimApell_KeyPress);
            // 
            // txtSegunApell
            // 
            this.txtSegunApell.Location = new System.Drawing.Point(171, 128);
            this.txtSegunApell.Name = "txtSegunApell";
            this.txtSegunApell.Size = new System.Drawing.Size(206, 22);
            this.txtSegunApell.TabIndex = 7;
            this.txtSegunApell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSegunApell_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Primer Apellido:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Segundo Apellido:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.txtTelef);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtSegunApell);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtPrimApell);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(21, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(406, 199);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Telefono";
            // 
            // txtTelef
            // 
            this.txtTelef.Location = new System.Drawing.Point(171, 164);
            this.txtTelef.Mask = "99999999";
            this.txtTelef.Name = "txtTelef";
            this.txtTelef.Size = new System.Drawing.Size(206, 22);
            this.txtTelef.TabIndex = 11;
            this.txtTelef.ValidatingType = typeof(int);
            // 
            // AñadCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(457, 296);
            this.Controls.Add(this.txtPrimNomb);
            this.Controls.Add(this.txtSegunNomb);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Name = "AñadCliente";
            this.Text = "AñadCliente";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPrimNomb;
        private System.Windows.Forms.TextBox txtSegunNomb;
        private System.Windows.Forms.TextBox txtPrimApell;
        private System.Windows.Forms.TextBox txtSegunApell;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox txtTelef;
    }
}