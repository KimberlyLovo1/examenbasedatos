﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class AñadCompra : Form
    {
        Validaciones v = new Validaciones();
        DateTime fech = DateTime.Now;
        public AñadCompra()
        {
            InitializeComponent();
            txtFecha.Text = fech.ToString();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(txtCant.Text.Equals("") || txtPrecio.Text.Equals(""))
            {
                MessageBox.Show("Ingrese los datos");
                return;
            }

            double subt = float.Parse(txtPrecio.Text) * float.Parse(txtCant.Text);
            for (int fila = 0; fila < dgvCompra.Rows.Count - 1; fila++)
            {
                if (dgvCompra.Rows[fila].Cells[0].Value.ToString() == cmbCodProd.SelectedItem.ToString())
                {
                    MessageBox.Show("Actualmente ya hay un producto con ese codigo, elimine y agregue otro");
                    return;
                }

            }
            dgvCompra.Rows.Add(cmbCodProd.SelectedItem.ToString(), txtPrecio.Text, txtCant.Text, subt.ToString());
            if (txtTotal.Text.Equals(""))
            {
                txtTotal.Text = subt.ToString();
            }
            else
            {
                subt = double.Parse(txtTotal.Text) + subt;
                txtTotal.Text = subt.ToString();
            }

        }

        public void loadCmb()
        {
            List<ListarProv_Result> proveedoresLista = Mostrar.MostrarProveedores();
            foreach (ListarProv_Result pl in proveedoresLista)
            {
                //Consideré que es más sencillo recordar el Id del proveedor si salía el nombre también. 
                cmbProv.Items.Add(pl.Id_Prov + " " + pl.NombreProv);
            }

            List<ListarTipoProd_Result> tipoProductoLista = Mostrar.TipoProd();
            foreach (ListarTipoProd_Result tp in tipoProductoLista)
            {
                cmbTipo.Items.Add(tp.NombreTipopro);
            }

        }
        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirNumerosDecimal(e);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvCompra.SelectedRows.Count > 0)
            {
                int cero = dgvCompra.SelectedRows[0].Index;

                double arreg = double.Parse(txtTotal.Text) - double.Parse(dgvCompra.Rows[cero].Cells[3].Value.ToString());

                txtTotal.Text = arreg.ToString();
                dgvCompra.Rows.RemoveAt(cero);

            }
            else
            {
                MessageBox.Show("Seleccione una fila");
                return;
            }
        }

        public void reset()
        {
            txtPrecio.Text = "";
            txtCant.Text = "";
        }
        public int idCom()
        {
            List<ListarCompras_Result> ec = Mostrar.MostrarCompra();
            int idcomp = 0;
            foreach (ListarCompras_Result ep in ec)
            {
                idcomp = ep.Id_Compra;
            }
            return idcomp;


        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dgvCompra.Rows.Count > 0)
            {
                int op = Insertar.InsertarCompra(cmbProv.SelectedItem.ToString(), cmbTipo.SelectedItem.ToString());
                if (op == 0)
                {
                    MessageBox.Show("error intente nuevamente");
                    return;
                }

                for (int fila = 0; fila < dgvCompra.Rows.Count - 1; fila++)
                {

                    Insertar.InsertDetalleComp(idCom(), dgvCompra.Rows[fila].Cells[0].Value.ToString(), Convert.ToInt32(dgvCompra.Rows[fila].Cells[2].Value.ToString()), float.Parse(dgvCompra.Rows[fila].Cells[1].Value.ToString()));
                }
                reset();
                MessageBox.Show("registrado correctamente");
                dgvCompra.Rows.Clear();
            }
        }
    }
}
