﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class AñadProducto : Form
    {
        Validaciones v = new Validaciones();
        public AñadProducto()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtExist_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirNumerosDecimal(e);
        }

        private void txtDirec_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(txtCodProd.Text.Equals("") || txtDirec.Text.Equals("") || txtNombre.Text.Equals("") || txtExist.Text.Equals("") || txtPrecio.Text.Equals("") || cmbProvee.SelectedIndex == -1 || cmbTipo.SelectedIndex == -1)
            {
                MessageBox.Show("Por favor, rellenar los campos.");
                return;
            }

            Insertar.InsertarProductos(txtCodProd.Text, txtNombre.Text, txtDirec.Text, float.Parse(txtPrecio.Text), Convert.ToInt32(txtExist.Text), cmbProvee.SelectedItem.ToString(), cmbTipo.SelectedIndex);
            reset();
        }

        public void reset()
        {
            txtCodProd.Text = "";
            txtDirec.Text = "";
            txtNombre.Text = "";
            txtPrecio.Text = "";
            txtExist.Text = "";
            cmbProvee.SelectedIndex = -1;
            cmbTipo.SelectedIndex = -1;
        }

        public void loadCmb()
        {
            List<ListarProv_Result> proveedoresLista = Mostrar.MostrarProveedores();
            foreach(ListarProv_Result pl in proveedoresLista)
            {
                //Consideré que es más sencillo recordar el Id del proveedor si salía el nombre también. 
                cmbProvee.Items.Add(pl.Id_Prov + " " + pl.NombreProv);
            }

            List<ListarTipoProd_Result> tipoProductoLista = Mostrar.TipoProd();
            foreach(ListarTipoProd_Result tp in tipoProductoLista)
            {
                cmbTipo.Items.Add(tp.NombreTipopro);
            }
        }
    }
}
