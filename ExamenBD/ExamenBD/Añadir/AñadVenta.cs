﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class AñadVenta : Form
    {
        DateTime fech = DateTime.Now;
        Validaciones v = new Validaciones();
        public AñadVenta()
        {
            InitializeComponent();
            txtFecha.Text = fech.ToString();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvVenta.SelectedRows.Count > 0)
            {
                int cero = dgvVenta.SelectedRows[0].Index;

                double arreg = double.Parse(txtTotal.Text) - double.Parse(dgvVenta.Rows[cero].Cells[3].Value.ToString());

                txtTotal.Text = arreg.ToString();
                dgvVenta.Rows.RemoveAt(cero);

            }
            else
            {
                MessageBox.Show("Seleccione una fila");
                return;
            }
        }
        public int idVent()
        {
            //Toma el último id de ventas
            List<ListarVentas_Result> ec = Mostrar.Ventas();
            int idcomp = 0;
            foreach (ListarVentas_Result ep in ec)
            {
                idcomp = ep.NVenta;
            }
            return idcomp;

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtCant.Text.Equals("") || float.Parse(txtExist.Text) < float.Parse(txtCant.Text))
            {
                MessageBox.Show("Error, intente nuevamente");
                return;
            }

            double preci = consultPrec(cmbCodProd.SelectedItem.ToString());
            double ex = preci * float.Parse(txtCant.Text);
            for (int fila = 0; fila < dgvVenta.Rows.Count - 1; fila++)
            {
                if (dgvVenta.Rows[fila].Cells[0].Value.ToString() == cmbCodProd.SelectedItem.ToString())
                {
                    MessageBox.Show("Actualmente ya hay un producto con ese codigo, elimine y agregue otro");
                    return;
                }
            }
            dgvVenta.Rows.Add(cmbCodProd.SelectedItem.ToString(), preci, txtCant.Text, ex.ToString());
            txtCant.Text = "";
            if (txtTotal.Text.Equals(""))
            {
                txtTotal.Text = ex.ToString();
            }
            else
            {
                ex = double.Parse(txtTotal.Text) + ex;
                txtTotal.Text = ex.ToString();
            }
        }
        
        public void LoadCmb()
        {
            List<ListarC_Result> clientesLista = Mostrar.MostrarClientes();
            foreach (ListarC_Result cl in clientesLista)
            {
                 
                cmbIdCliente.Items.Add(cl.Id_cliente + " " + cl.PNombre);
            }

            List<ListarProduct_Result> ProductoLista = Mostrar.MostrarProductos();
            foreach (ListarProduct_Result pl in ProductoLista)
            {
                cmbCodProd.Items.Add(pl.Cod_Prod);
            }
        }

        public double consultPrec(string codpro)
        {
            double precio = 0.00;
            List<ListarProduct_Result> list = Mostrar.MostrarProductos();

            foreach (ListarProduct_Result p in list)
            {
                if (codpro.Equals(p.Cod_Prod))
                {
                    precio = p.precio;
                }
            }
            return precio;
        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dgvVenta.Rows.Count > 0)
            {
                int opl = 0;
                int op = Insertar.InsertarVenta(cmbIdCliente.SelectedIndex + 1);
                if (op == 0)
                {
                    MessageBox.Show("error intente nuevamente");
                    return;
                }

                for (int fila = 0; fila < dgvVenta.Rows.Count - 1; fila++)
                {

                    opl = Insertar.InsertarDetalleVent(idVent(), dgvVenta.Rows[fila].Cells[0].Value.ToString(), Convert.ToInt32(dgvVenta.Rows[fila].Cells[2].Value.ToString()));

                }
                txtCant.Text = "";
                MessageBox.Show("registrado correctamente");
                dgvVenta.Rows.Clear();
                txtExist.Text = actExist(cmbCodProd.SelectedItem.ToString());
            }
        }

        public string actExist(String codpros)
        {
            List<ListarProduct_Result> list = Mostrar.MostrarProductos();
            int exist = 0;
            foreach (ListarProduct_Result ok in list)
            {
                if (codpros.Equals(ok.Cod_Prod))
                {
                    exist = ok.exist;
                }
            }
            return exist.ToString();

        }

        private void cmbCodProd_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtExist.Text = actExist(cmbCodProd.SelectedItem.ToString());
        }

        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }
    }
}
