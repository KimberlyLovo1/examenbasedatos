﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class AñadProveedor : Form
    {
        Validaciones v = new Validaciones();
        public AñadProveedor()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(txtIdProv.Text.Equals("") || txtNombre.Text.Equals("") || txtDireccion.Text.Equals("") || txtTelefono.Text.Equals("") || txtCorreo.Text.Equals(""))
            {
                MessageBox.Show("Por favor, rellenar los campos.");
                return;
            }
            char[] num = txtTelefono.Text.ToCharArray();
            if (num.Length != 8)
            {
                MessageBox.Show("estructura de numeros incorrecta");
                txtTelefono.Text = "";
                return;
            }
            if (num[0] != '2' && num[0] != '8' && num[0] != '7' && num[0] != '5')
            {
                MessageBox.Show("estructura de numeros incorrecta");
                txtTelefono.Text = "";
                return;

            }
            if (txtIdProv.Text.Length != 4)
            {
                MessageBox.Show("estructura de codigo incorrecta");
                return;
            }

            Insertar.InsertarProveedor(txtIdProv.Text, txtNombre.Text, txtTelefono.Text, txtDireccion.Text, txtCorreo.Text);
            reset();
        }

        public void reset()
        {
            txtCorreo.Text = "";
            txtDireccion.Text = "";
            txtIdProv.Text = "";
            txtNombre.Text = "";
            txtTelefono.Text = "";
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Se cancelax3 :'v
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }
    }
}
