﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class AñadCliente : Form
    {
        Validaciones v = new Validaciones();
        public AñadCliente()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if(txtPrimApell.Text.Equals("") || txtPrimNomb.Text.Equals("") || txtSegunApell.Text.Equals("") || txtSegunNomb.Text.Equals("") || txtTelef.Text.Equals(""))
            {
                MessageBox.Show("Por favor, rellenar los campos.");
                return;
            }
            Insertar.InsertarCliente(txtPrimNomb.Text, txtSegunNomb.Text, txtSegunNomb.Text, txtSegunApell.Text, txtTelef.Text);
            reset();
        }
        
        public void reset()
        {
            txtPrimNomb.Text = "";
            txtSegunNomb.Text = "";
            txtPrimApell.Text = "";
            txtSegunApell.Text = "";
            txtTelef.Text = "";
        }

        private void txtPrimNomb_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtSegunNomb_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtPrimApell_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtSegunApell_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void txtTelef_KeyPress(object sender, KeyPressEventArgs e)
        {
            //v.AdmitirEnteros(e), lo deje como mask ya ni es necesario xd
        }
    }
}
