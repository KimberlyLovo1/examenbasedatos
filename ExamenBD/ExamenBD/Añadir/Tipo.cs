﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenBD.BaseDatos;

namespace ExamenBD.Añadir
{
    public partial class Tipo : Form
    {
        Validaciones v = new Validaciones();
        public Tipo()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtAceptar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text.Equals(""))
            {
                MessageBox.Show("Ingrese el nombre");
                return;
            }
            Insertar.InsertarTipoProd(txtNombre.Text);
            txtNombre.Text = "";
        }
    }
}
