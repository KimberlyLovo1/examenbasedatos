﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenBD.Reporte
{
    public partial class view2 : Form
    {
        public view2()
        {
            InitializeComponent();
        }

        private void view2_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'bDExamenDataSet.ProducutoMasVendido' Puede moverla o quitarla según sea necesario.
            this.producutoMasVendidoTableAdapter.Fill(this.bDExamenDataSet.ProducutoMasVendido);

            this.reportViewer1.RefreshReport();
        }
    }
}
