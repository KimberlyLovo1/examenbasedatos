﻿namespace ExamenBD.Reporte
{
    partial class view2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bDExamenDataSet = new ExamenBD.BDExamenDataSet();
            this.producutoMasVendidoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.producutoMasVendidoTableAdapter = new ExamenBD.BDExamenDataSetTableAdapters.ProducutoMasVendidoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bDExamenDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.producutoMasVendidoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.producutoMasVendidoBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ExamenBD.Reporte.Report2.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // bDExamenDataSet
            // 
            this.bDExamenDataSet.DataSetName = "BDExamenDataSet";
            this.bDExamenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // producutoMasVendidoBindingSource
            // 
            this.producutoMasVendidoBindingSource.DataMember = "ProducutoMasVendido";
            this.producutoMasVendidoBindingSource.DataSource = this.bDExamenDataSet;
            // 
            // producutoMasVendidoTableAdapter
            // 
            this.producutoMasVendidoTableAdapter.ClearBeforeFill = true;
            // 
            // view2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "view2";
            this.Text = "view2";
            this.Load += new System.EventHandler(this.view2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bDExamenDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.producutoMasVendidoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private BDExamenDataSet bDExamenDataSet;
        private System.Windows.Forms.BindingSource producutoMasVendidoBindingSource;
        private BDExamenDataSetTableAdapters.ProducutoMasVendidoTableAdapter producutoMasVendidoTableAdapter;
    }
}