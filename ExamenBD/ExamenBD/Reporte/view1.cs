﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenBD.Reporte
{
    public partial class view1 : Form
    {
        public view1()
        {
            InitializeComponent();
        }

        private void view1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'bDExamenDataSet.Ventasporcliente' Puede moverla o quitarla según sea necesario.
            this.ventasporclienteTableAdapter.Fill(this.bDExamenDataSet.Ventasporcliente);

            this.reportViewer1.RefreshReport();
        }
    }
}
