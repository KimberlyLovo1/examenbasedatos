﻿namespace ExamenBD.Reporte
{
    partial class view1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bDExamenDataSet = new ExamenBD.BDExamenDataSet();
            this.ventasporclienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ventasporclienteTableAdapter = new ExamenBD.BDExamenDataSetTableAdapters.VentasporclienteTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bDExamenDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ventasporclienteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.ventasporclienteBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ExamenBD.Reporte.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // bDExamenDataSet
            // 
            this.bDExamenDataSet.DataSetName = "BDExamenDataSet";
            this.bDExamenDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ventasporclienteBindingSource
            // 
            this.ventasporclienteBindingSource.DataMember = "Ventasporcliente";
            this.ventasporclienteBindingSource.DataSource = this.bDExamenDataSet;
            // 
            // ventasporclienteTableAdapter
            // 
            this.ventasporclienteTableAdapter.ClearBeforeFill = true;
            // 
            // view1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "view1";
            this.Text = "view1";
            this.Load += new System.EventHandler(this.view1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bDExamenDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ventasporclienteBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private BDExamenDataSet bDExamenDataSet;
        private System.Windows.Forms.BindingSource ventasporclienteBindingSource;
        private BDExamenDataSetTableAdapters.VentasporclienteTableAdapter ventasporclienteTableAdapter;
    }
}